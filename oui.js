document.addEventListener('DOMContentLoaded', ()=>
{
  const CASES = document.querySelectorAll('.case');

  var rand = Math.round(Math.random()),
    rouges = 8,
    bleues = 8,
    noire = 1;

  if(rand == 0)
  {
    rouges++;
    document.body.style.setProperty('background', 'rgba(165, 38, 10, 0.3)');
  }
  else
  {
    bleues++;
    document.body.style.setProperty('background', 'rgba(1, 49, 180, 0.3)');
  }

  CASES[Math.floor(Math.random() * 25)].classList.add('noire');

  while(rouges + bleues > 0)
  {
    rand = Math.floor(Math.random() * 25);

    if(!CASES[rand].classList.contains('noire') && !CASES[rand].classList.contains('rouge') && !CASES[rand].classList.contains('bleue'))
    {
      if((rouges + bleues) % 2 == 0)
      {
        if(rouges > bleues)
        {
          CASES[rand].classList.add('rouge');
          rouges--;
        }
        else
        {
          CASES[rand].classList.add('bleue');
          bleues--;
        }
      }
      else
      {
        if(bleues > rouges)
        {
          CASES[rand].classList.add('bleue');
          bleues--;
        }
        else
        {
          CASES[rand].classList.add('rouge');
          rouges--;
        }
      }
    }
  }
});
